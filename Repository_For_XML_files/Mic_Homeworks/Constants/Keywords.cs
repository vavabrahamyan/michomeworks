﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository_For_XML_files.Constants
{
    static class Keywords
    {
        public const string ElName = "student";
        public const string FirstName = "firstname";
        public const string LastName = "lastname";
        public const string BirthDate = "birthday";
    }
}
