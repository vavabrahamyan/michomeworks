﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository_For_XML_files.Models
{
    class Student
    {

        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public DateTime BirthDay { get; set; }

        public int Age
        {
            get
            {
                TimeSpan age = DateTime.Now - BirthDay;
                return age.Days / 365;
            }
        }

        public override string ToString()
        {
            return $"{FirstName}\t{Lastname}\t{Age}";
        }
    }
}
