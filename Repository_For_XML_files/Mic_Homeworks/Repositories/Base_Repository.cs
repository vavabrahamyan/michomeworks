﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Repository_For_XML_files
{
    public abstract class BaseRepository<TModel> where TModel : class
    {
        public BaseRepository(string fileName)
        {
            FileName = fileName;
        }

        public string FileName { get; }

        public IEnumerable<TModel> AsEnumerable()
        {
            var XDoc = new XmlDocument();
            XDoc.Load(FileName);
            var xRoot = XDoc.DocumentElement;

            foreach (XmlNode xnode in xRoot.ChildNodes)
            {
                yield return ToModel(xnode);
            }
        }

        protected abstract TModel ToModel(XmlNode xnode);
    }
}
