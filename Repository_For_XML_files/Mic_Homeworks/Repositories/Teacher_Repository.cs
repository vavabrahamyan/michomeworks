﻿using Repository_For_XML_files.Models;
using Repository_For_XML_files.Constants;
using System.Xml;
using System;

namespace Repository_For_XML_files.Repositories
{
    class Teacher_Repository:BaseRepository<Teacher>
    {
        public Teacher_Repository(string filerName) : base(filerName) { }

        protected override Teacher ToModel(XmlNode xnode)
        {
            var item = new Teacher();
            foreach(XmlNode xchild in xnode.ChildNodes)
            {
                switch (xchild.Name)
                {
                    case Keywords.FirstName:
                        item.FirstName = xchild.InnerText;
                        break;

                    case Keywords.LastName:
                        item.Lastname = xchild.InnerText;
                        break;

                    case Keywords.BirthDate:
                        if (DateTime.TryParse(xchild.InnerText, out DateTime date))
                        {
                            item.BirthDay = DateTime.Parse(xchild.InnerText);
                        }
                        break;
                }
            }
            return item;
        }
    }
}
